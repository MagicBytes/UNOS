#include "main.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* Check if the compiler thinks we are targeting the wrong operating system. */
#if defined(__linux__)
#error                                                                         \
    "You are not using a cross-compiler, you will most certainly run into trouble"
#endif

/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
#error "This kernel needs to be compiled with a ix86-elf compiler"
#endif

#define TRUE 0x01
#define FALSE 0x00

const char *str = "$> ";

/* Hardware text mode color constants. */
enum vga_color {
  VGA_COLOR_BLACK = 0,
  VGA_COLOR_BLUE = 1,
  VGA_COLOR_GREEN = 2,
  VGA_COLOR_CYAN = 3,
  VGA_COLOR_RED = 4,
  VGA_COLOR_MAGENTA = 5,
  VGA_COLOR_BROWN = 6,
  VGA_COLOR_LIGHT_GREY = 7,
  VGA_COLOR_DARK_GREY = 8,
  VGA_COLOR_LIGHT_BLUE = 9,
  VGA_COLOR_LIGHT_GREEN = 10,
  VGA_COLOR_LIGHT_CYAN = 11,
  VGA_COLOR_LIGHT_RED = 12,
  VGA_COLOR_LIGHT_MAGENTA = 13,
  VGA_COLOR_LIGHT_BROWN = 14,
  VGA_COLOR_WHITE = 15,
};

static inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) {
  return fg | bg << 4;
}

static inline uint16_t vga_entry(unsigned char uc, uint8_t color) {
  return (uint16_t)uc | (uint16_t)color << 8;
}

size_t strlen(const char *str) {
  size_t len = 0;
  while (str[len]) {
    len++;
  }
  return len;
}

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t *terminal_buffer;

void terminal_initialize(void) {
  terminal_row = 0;
  terminal_column = 0;
  terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
  terminal_buffer = (uint16_t *)0xB8000;
  for (size_t y = 0; y < VGA_HEIGHT; y++) {
    for (size_t x = 0; x < VGA_WIDTH; x++) {
      const size_t index = y * VGA_WIDTH + x;
      terminal_buffer[index] = vga_entry(' ', terminal_color);
    }
  }
}

void terminal_setcolor(uint8_t color) { terminal_color = color; }

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) {
  const size_t index = y * VGA_WIDTH + x;
  terminal_buffer[index] = vga_entry(c, color);
}

void terminal_putchar(char c) {
  if (c == '\n') {
    terminal_row++;
    terminal_column = 0;
    if (terminal_row == 25) {
      terminal_row = 0;
    }
    return;
  }
  terminal_putentryat(c, terminal_color, terminal_column++, terminal_row);
  if (terminal_column == 80) {
    terminal_column = 0;
  }
}

void terminal_write(const char *data, size_t size) {
  for (size_t i = 0; i < size; i++) {
    terminal_putchar(data[i]);
  }
}

void terminal_writestring(const char *data) {
  terminal_write(data, strlen(data));
}

UINT8 IN_B(UINT16 port) {
  UINT8 ret;
  asm volatile("inb %1, %0" : "=a"(ret) : "Nd"(port));
  return ret;
}

char getInputCode() {
  char ch = 0;
  do {
    if (IN_B(0x60) != ch) {
      ch = IN_B(0x60);
      if (ch > 0) {
        return ch;
      }
    }
  } while (1);
}

void print_welcome_screen() {
  terminal_writestring(
      "Welcome to UNOS (by Pascal Peinecke)!\nVersion 1.1\n\n");

  terminal_setcolor(VGA_COLOR_GREEN);
  terminal_writestring("888     888 888b    888  .d88888b.   .d8888b.\n");
  terminal_writestring("888     888 8888b   888 d88P   Y88b d88P  Y88b\n");
  terminal_writestring("888     888 88888b  888 888     888 Y88b.\n");
  terminal_writestring("888     888 888Y88b 888 888     888   Y888b.\n");
  terminal_writestring("888     888 888 Y88b888 888     888      Y88b.\n");
  terminal_writestring("888     888 888  Y88888 888     888        888\n");
  terminal_writestring("Y88b. .d88P 888   Y8888 Y88b. .d88P Y88b  d88P\n");
  terminal_writestring("  Y88888P   888    Y888   Y88888P     Y8888P\n");
}

void clearScreen() {
  __asm__("int $0x10" : : "a"(0x0200), "b"(0x0000), "d"(0x0000));
  __asm__("int $0x10" : : "a"(0x0920), "b"(0x0007), "c"(0x2000));
}

void showCursor(short choice) {
  if (choice == FALSE) {
    __asm__("int $0x10" : : "a"(0x0100), "c"(0x3200));
  } else {
    __asm__("int $0x10" : : "a"(0x0100), "c"(0x0007));
  }
}

short getchar() {
  char ch;

  ch = getInputCode();

  return ch;
}

void putchar(short ch) { terminal_putchar(ch); }

void printString(const char *pStr) {
  while (*pStr) {
    __asm__("int $0x10" : : "a"(0x0e00 | *pStr), "b"(0x0002));
    ++pStr;
  }
}

static void output_line(char *line) { printString(line); }

static void output_prompt() { output_line(">"); }

static int input_line_waiting() {
  // TODO: perhaps someone more familiar with the C library could write a better
  // example?
}

static char *input_line() {
  // TODO: perhaps someone more familiar with the C library could write a better
  // example?
  // TODO: this function will allocate a char buffer containing the line read
}

static int process_start(char *command) {
  // exec(command); todo: implement exec function
  // TODO: needs to return an int identifying this process
}

static int process_executing(int proc) {
  // TODO: what would be a good example here?
}

static void process_send_input_line(int proc, char *line) {
  // TODO: what would be a good example here?
}

static int process_output_line_waiting(int proc) {
  // TODO: what would be a good example here?
}

static char *process_get_output_line(int proc) {
  // TODO: what would be a good example here?
  // TODO: this function will allocate a char buffer containing the line read
}

void shell() {
  // clearScreen();
  // showCursor(TRUE);
  while (true) { // you may want to provide a built-in "exit" command
    char *command;
    int proc;
    output_prompt();               // display a prompt
    command = input_line();        // get a line of input, which will become the
                                   // command to execute
    proc = process_start(command); // start a process from the command
  //  free(command);	// TODO: implement free() function

    while (process_executing(proc)) {
      if (input_line_waiting()) {
        char *line;
        line = input_line();                 // read input from user
        process_send_input_line(proc, line); // send input to process
      //  free(line);
      }
      if (process_output_line_waiting(proc)) {
        char *output;
        output = process_get_output_line(proc); // get output from process
        output_line(output);                    // write output to user
      //  free(output);			// TODO: implement free() function
      }
    }
  }
}

void kernel_main(void) {

  /* Initialize terminal interface */
  terminal_initialize();

  print_welcome_screen();

  shell();	// enter userland shell

  while (1);	// keep kernel running

}
